# sepomex-api

**sepomex-api** is an opinionated Rails REST Api to look up the data from the current zipcodes in Mexico. Data used on this app is gattered from a CSV by the official SEPOMEX database that you can find and download [here](http://www.sepomex.gob.mx/lservicios/servicios/CodigoPostal_Exportar.aspx)

If you are using this API to query the zipcodes on a Rails App I recomend checking the [sepomex-client](https://gitlab.com/aromaron/sepomex-client) a ruby gem I've built to consume the endpoints. 

sepomex-api is currently hosted on heroku: [http://sepomex.herokuapp.com/api/v1/zipcodes](http://sepomex.herokuapp.com/api/v1/zipcodes)

## Dependencies

**sepomex-api** is built on Ruby 2.5.1 and Rails 5.2.1

## Usage of API

### Zipcodes

#### All:
Returns all the zipcodes.
```
http://sepomex.herokuapp.com/api/v1/zipcodes
```

#### Search:
Returns the zipcode data that matches the search params.

All matching zipcodes.
```
http://sepomex.herokuapp.com/api/v1/zipcodes/search?code=28979
```

A zipcode from a neighborhood.
```
http://sepomex.herokuapp.com/api/v1/zipcodes/search?neighborhood=Colima
```

All zipcodes in a given municipality.
```
http://sepomex.herokuapp.com/api/v1/zipcodes/search?municipality=Colima
```

All zipcodes in a given city.
```
http://sepomex.herokuapp.com/api/v1/zipcodes/search?city=Colima
```

All zipcodes in a given state.
```
http://sepomex.herokuapp.com/api/v1/zipcodes/search?state=Colima
```

### Municipalities

#### All:
Returns all the municipalities.
```
http://sepomex.herokuapp.com/api/v1/municipalities
```

#### Specific Municipality
Returns a specific municipality based on its Id or Name.

```
http://sepomex.herokuapp.com/api/v1/municipalities/1

http://sepomex.herokuapp.com/api/v1/municipalities/Tecomán
```

### Cities

#### All:
Returns all the cities.
```
http://sepomex.herokuapp.com/api/v1/cities
```

#### Specific City
Returns a specific city based on its Id or Name.

```
http://sepomex.herokuapp.com/api/v1/cities/1

http://sepomex.herokuapp.com/api/v1/cities/Colima
```

### States

#### All:
Returns all the states.
```
http://sepomex.herokuapp.com/api/v1/states
```

#### Specific State
Returns a specific state based on its Id or Name.

```
http://sepomex.herokuapp.com/api/v1/states/1

http://sepomex.herokuapp.com/api/v1/states/Colima
```

#### Specific State Municipalities
Returns all municipalities from a state based on its Id or Name.

```
http://sepomex.herokuapp.com/api/v1/states/1/municipalities

http://sepomex.herokuapp.com/api/v1/states/Colima/municipalities
```


Note: currently finding a State, City or Municipality by its name is case sensitive, so please be aware of the use of capital letters and other special characters (like accents).

## Setup

If you want to use it locally or setup your own API

```ruby
# clone, bundle, and create db
$ git clone
$ cd sepomex-api
$ bundle install
$ bundle exec rails db:create
$ bundle exec rails db:migrate

# To load all data from Sepomex CSV run
$ bundle exec rake import:sepomex:all

# To load just specific set of data from de Sepomex CSV
$ bundle exec rake import:sepomex:zipcodes
$ bundle exec rake import:sepomex:states # requires zipcodes to be already created
$ bundle exec rake import:sepomex:cities # requires states to be already created
$ bundle exec rake import:sepomex:municipalities # requires states to be already created

# Launch your server
$ bundle exec rails server
```

## Tests

```ruby
$ bundle exec rails test
```

## Contributing

Bug reports and pull requests are welcome, please refer to the [Contribution Guidelines](https://gitlab.com/aromaron/sepomex-api/blob/master/CONTRIBUTING.md).

## License

The app is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the sepomex-api project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [Code of Conduct](https://gitlab.com/aromaron/sepomex-api/blob/master/CODE_OF_CONDUCT.md).


## Aknowledgments

**sepomex-api** is heavily inspired and influenced by the awesome work on the [Sepomex](https://github.com/IcaliaLabs/sepomex) api.
