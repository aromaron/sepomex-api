# Contributing

If you discover issues, have ideas for improvements or new features,
please report them to the issue tracker of the repository or
submit a pull request. Please, try to follow these guidelines when you
do so.

## Issue reporting

* Check that the issue has not already been reported.
* Check that the issue has not already been fixed in the latest code
  (a.k.a. `master`).

## Pull requests

* Fork the project.
* Use a topic/feature branch to easily amend a pull request later, if necessary.
* Write good commit messages.
* Use the same coding conventions as the rest of the project.
* Commit and push until you are happy with your contribution.
* Make sure to add tests for it.
* Make sure the test suite is passing and the code you wrote doesn't produce
  RuboCop offenses.
* Squash related commits together.
* Open a pull request that relates to *only* one subject with a clear title
  and description in grammatically correct, complete sentences.